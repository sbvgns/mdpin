package com.massdyn.mdpin.api.converter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.massdyn.mdpin.R;
import com.massdyn.mdpin.model.Pin;

import java.util.ArrayList;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {

    public interface OnItemClickListener {
        void onItemClick(Pin pin);
    }

    Context context;
    private ArrayList<Pin> pins;
    private final OnItemClickListener listener;

    public DataAdapter(Context context, ArrayList<Pin> pins, OnItemClickListener listener) {
        this.context = context;
        this.pins = pins;
        this.listener = listener;
    }

    @Override
    public DataAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_row,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DataAdapter.ViewHolder holder, int position) {
        Pin pin = pins.get(position);
        holder.tv_title.setText(pin.getTitle());

        if (pin.getType().equals(Pin.STR_TYPE_IMAGE)) {
            Glide.with(context)
                    .load(pin.getContentUrl())
                    .into(holder.iv_thumbnail);
        }
        holder.bind(pin, listener);
    }

    @Override
    public int getItemCount() {
        return pins.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_title;
        private ImageView iv_thumbnail;

        public ViewHolder(View itemView) {
            super(itemView);

            tv_title = (TextView)itemView.findViewById(R.id.tv_title);
            iv_thumbnail = (ImageView)itemView.findViewById(R.id.iv_thumbnail);

        }

        public void bind(final Pin pin, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(pin);
                }
            });
        }
    }


}