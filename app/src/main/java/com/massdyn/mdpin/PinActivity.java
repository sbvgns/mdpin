package com.massdyn.mdpin;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.massdyn.mdpin.api.converter.DataAdapter;
import com.massdyn.mdpin.api.request.PinRequest;
import com.massdyn.mdpin.api.request.PinsRequest;
import com.massdyn.mdpin.model.Pin;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PinActivity extends AppCompatActivity {
    private String TAG = "PinActivity";

    private Context context;

    private TextView mDescriptionView;
    private TextView mTitleView;
    private TextView mDateView;
    private ImageView mThumbnailView;
    private Button mLinkView;

    private Pin mPin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin);

        context = this;

        mDescriptionView = findViewById(R.id.tv_description);
        mTitleView = findViewById(R.id.tv_title);
        mDateView = findViewById(R.id.tv_date);
        mThumbnailView = findViewById(R.id.iv_thumbnail);
        mLinkView = findViewById(R.id.bt_link);
        mLinkView.setEnabled(false);

        Bundle bundle = getIntent().getExtras();
        if(0 != bundle.getInt("pinId")) {
            int pinId = bundle.getInt("pinId");
            getPin(pinId);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.menu_scrolling, menu);
        return true;
    }

    public void LinkButtonHandler(View target) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(mPin.getLinkUrl()));
        startActivity(browserIntent);
    }

    public void updateView() {
        mDateView.setText(mPin.getDate());
        mTitleView.setText(mPin.getTitle());
        mDescriptionView.setText(mPin.getDescription());
        mLinkView.setEnabled(true);

        Glide.with(context)
                .load(mPin.getContentUrl())
                .into(mThumbnailView);
    }

    public Pin getPin(int pinId) {
        PinRequest pinRequest = new PinRequest();
        pinRequest.setWsToken(ScrollingActivity.token.getWsToken());
        pinRequest.setPinId(pinId);

        ScrollingActivity.mAPIService.savePin(pinRequest.toJson()).enqueue(new Callback<Pin>() {
            @Override
            public void onResponse(Call<Pin> call, Response<Pin> response) {
                if(response.isSuccessful()) {
                    mPin = (Pin) response.body();
                    updateView();
                }
            }

            @Override
            public void onFailure(Call<Pin> call, Throwable t) {
                Log.e(TAG, "Unable to submit post to API : " + t.getMessage());
            }
        });
        return null;
    }
}
