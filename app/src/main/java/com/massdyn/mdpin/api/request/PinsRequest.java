package com.massdyn.mdpin.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PinsRequest {

    @SerializedName("wsToken")
    @Expose
    private String wsToken;

    @SerializedName("index")
    @Expose
    private int index;

    public PinsRequest() {
        index = 0;
    }

    public String getWsToken() {
        return wsToken;
    }

    public void setWsToken(String wsToken) {
        this.wsToken = wsToken;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    @Override
    public String toString() {
        return "PinsRequest{" +
                "wsToken='" + wsToken + '\'' +
                ", index='" + String.valueOf(index) + '\'' +
                '}';
    }

    public String toJson() {
        return "{" +
                "\"wsToken\":\"" + wsToken + '\"' +
                ", \"index\":\"" + String.valueOf(index) + '\"' +
                '}';
    }
}