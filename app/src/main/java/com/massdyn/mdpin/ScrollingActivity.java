package com.massdyn.mdpin;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.massdyn.mdpin.api.APIService;
import com.massdyn.mdpin.api.converter.DataAdapter;
import com.massdyn.mdpin.api.request.PinsRequest;
import com.massdyn.mdpin.model.AuthLogin;
import com.massdyn.mdpin.model.AuthToken;
import com.massdyn.mdpin.model.Pin;
import com.massdyn.mdpin.model.Pins;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScrollingActivity extends AppCompatActivity {
    private String TAG = "ScrollingActivity";
    static AuthToken token;
    static APIService mAPIService;
    public TextView tvLargeText;
    public ArrayList<Pin> pins;;
    private RecyclerView recyclerView;
    private DataAdapter adapter;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling);

        context = this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        recyclerView = (RecyclerView)findViewById(R.id.card_recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);


        tvLargeText = (TextView) findViewById(R.id.tvLargeText);
        if ( null != token && null != token.getWsToken() && null == getPins()) {
            tvLargeText.setText("Please log in ! ! !");
        }

    }

    @Override
    protected void onResume() {

        super.onResume();

        if ( null != token && null != token.getWsToken()) {
            if (null == getPins() )tvLargeText.setText("Please log in ! ! !");
            else {
                tvLargeText.setText(pins.toString());
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_scrolling, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent intObj = new Intent(ScrollingActivity.this,LoginActivity.class);

        if (id == R.id.action_settings) {
            startActivity(intObj);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public List<Pin> getPins() {
        PinsRequest pinsRequest = new PinsRequest();
        pinsRequest.setWsToken(token.getWsToken());

        ScrollingActivity.mAPIService.savePins(pinsRequest.toJson()).enqueue(new Callback<List<Pin>>() {
            @Override
            public void onResponse(Call<List<Pin>> call, Response<List<Pin>> response) {
                if(response.isSuccessful()) {
                    pins = (ArrayList<Pin>) response.body();
                    adapter = new DataAdapter(context, pins, new DataAdapter.OnItemClickListener() {
                        @Override public void onItemClick(Pin item) {
                            Intent intent = new Intent(ScrollingActivity.this,PinActivity.class);
                            intent.putExtra("pinId", item.getId());
                            startActivity(intent);
                        }
                    });
                    recyclerView.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<List<Pin>> call, Throwable t) {
                Log.e(TAG, "Unable to submit post to API : " + t.getMessage());
            }
        });
        return null;
    }
}
