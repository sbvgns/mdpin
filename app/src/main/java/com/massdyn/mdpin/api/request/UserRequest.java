package com.massdyn.mdpin.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserRequest {

    @SerializedName("wsToken")
    @Expose
    private String wsToken;

    @SerializedName("userId")
    @Expose
    private int userId;

    public UserRequest() {
        userId = 0;
    }

    public String getWsToken() {
        return wsToken;
    }

    public void setWsToken(String wsToken) {
        this.wsToken = wsToken;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "PinsRequest{" +
                "wsToken='" + wsToken + '\'' +
                ", userId='" + String.valueOf(userId) + '\'' +
                '}';
    }

    public String toJson() {
        return "{" +
                "\"wsToken\":\"" + wsToken + '\"' +
                ", \"userId\":\"" + String.valueOf(userId) + '\"' +
                '}';
    }
}