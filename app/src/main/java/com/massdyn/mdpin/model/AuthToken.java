package com.massdyn.mdpin.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AuthToken {

    @SerializedName("wsToken")
    @Expose
    private String wsToken;

    @SerializedName("refreshToken")
    @Expose
    private String refreshToken;

    public String getWsToken() {
        return wsToken;
    }

    public void setWsToken(String wsToken) {
        this.wsToken = wsToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    @Override
    public String toString() {
        return "AuthToken{" +
                "wsToken='" + wsToken + '\'' +
                ", wsRefreshToken='" + refreshToken + '\'' +
                '}';
    }
}
