package com.massdyn.mdpin.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Pins {

    @SerializedName("pins")
    @Expose
    private ArrayList<Pin> pins;

    public Pins() {
        pins = new ArrayList<>();
    }

    public ArrayList<Pin> getPins() {
        return pins;
    }

    public void setPins(ArrayList<Pin> pins) {
        this.pins = pins;
    }

    @Override
    public String toString() {
        String result = "Pins{";
        for (int i=0; i<getPins().size(); i++){
            result = result + getPins().get(i).toString();
        }
        return result + '}';
    }

}
