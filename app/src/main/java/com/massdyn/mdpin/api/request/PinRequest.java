package com.massdyn.mdpin.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PinRequest {

    @SerializedName("wsToken")
    @Expose
    private String wsToken;

    @SerializedName("pinId")
    @Expose
    private int pinId;

    public PinRequest() {
        pinId = 0;
    }

    public String getWsToken() {
        return wsToken;
    }

    public void setWsToken(String wsToken) {
        this.wsToken = wsToken;
    }

    public int getPinId() {
        return pinId;
    }

    public void setPinId(int pinId) {
        this.pinId = pinId;
    }

    @Override
    public String toString() {
        return "PinsRequest{" +
                "wsToken='" + wsToken + '\'' +
                ", pinId='" + String.valueOf(pinId) + '\'' +
                '}';
    }

    public String toJson() {
        return "{" +
                "\"wsToken\":\"" + wsToken + '\"' +
                ", \"pinId\":\"" + String.valueOf(pinId) + '\"' +
                '}';
    }
}