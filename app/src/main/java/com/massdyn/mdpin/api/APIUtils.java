package com.massdyn.mdpin.api;

import android.util.Log;

import com.massdyn.mdpin.api.APIService;
import com.massdyn.mdpin.api.RetrofitClient;

public class APIUtils {

    private APIUtils() {}

    public static final String BASE_URL = "http://challenge.synertic.net/";

    public static APIService getAPIService() {
        Log.i("Retrofit", "Init ! ! !");
        return RetrofitClient.getClient(BASE_URL).create(APIService.class);
    }
}
