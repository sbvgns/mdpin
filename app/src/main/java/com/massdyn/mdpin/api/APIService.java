package com.massdyn.mdpin.api;

import com.massdyn.mdpin.model.AuthToken;
import com.massdyn.mdpin.model.Pin;
import com.massdyn.mdpin.model.Pins;
import com.massdyn.mdpin.model.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface APIService {

    @POST("/services/request/authentification")
    Call<AuthToken> saveAuthToken(@Body String login);

    @POST("/services/request/getPins")
    Call<List<Pin>> savePins(@Body String pins);

    @POST("/services/request/getPinById")
    Call<Pin> savePin(@Body String pin);

    @POST("/services/request/getUserById")
    Call<User> saveUser(@Body String user);

}